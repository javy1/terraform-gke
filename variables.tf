variable "name" {
  default = "ripley-k8s-cluster"
}

variable "project" {
  default = "semiotic-mender-353921"
}

variable "location" {
  default = "us-east1-b"
}

variable "initial_node_count" {
  default = 3
}

variable "machine_type" {
  default = "n1-standard-1"
}

